<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    public function login(Request $request)
    {
    	$request->validate([
    		'email' => 'required|email',
    		'password' => 'required'
    	]);

    	$new_request = Request::create('oauth/token', 'POST', [
            'client_id' => env('PASSPORT_PASSWORD_GRANT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_PASSWORD_GRANT_CLIENT_SECRET'),
            'username' => $request->email,
            'password' => $request->password,
            'grant_type' => 'password',
            'scope' => ''
        ]);

        $new_request->headers->set('Origin', '*');

        return app()->handle($new_request);
    }

    public function logout()
    {
        auth()->guard('api')->user()->token()->revoke();

        return 1;
    }
}
