<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Suppliers;
use App\Models\SupplierProducts;
use Illuminate\Http\Request;
use App\Http\Resources\SuppliersResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $suppliersQuery = SuppliersResource::collection(Suppliers::all());
        return [
            'suppliers' => $suppliersQuery
        ];

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $suppliers = Suppliers::create([
            'name' => $params['name'],

        ]);
        new SuppliersResource($suppliers);
        return response()->json("Supplier has been added Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\suppliers  $suppliers
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $suppliers=suppliers::findorfail($id);
        return new SuppliersResource($suppliers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\suppliers  $suppliers
     * @return \Illuminate\Http\Response
     */
    public function edit(suppliers $suppliers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\suppliers  $suppliers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($id === null) {
            return response()->json(['error' => 'suppliers not found'], 404);
        }else{
            $suppliers=Suppliers::findorfail($id);
                $params = $request->all();
              $suppliers->update($params);
          new SuppliersResource($suppliers);

          return response()->json("Supplier has been updated Successfully");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\suppliers  $suppliers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suppliers=Suppliers::find($id);
        $orders = SupplierProducts::where('supplier_id',$id)->delete();

        if ($suppliers->delete()) {
            return response()->json('Supplier deleted Succefully');
        } else {
            abort(404,'Supplier not found.');
        }
    }
}
