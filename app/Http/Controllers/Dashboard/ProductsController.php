<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\SupplierProducts;
use App\Models\Orders;
use App\Models\Suppliers;
use App\Models\OrderDetails;
use Illuminate\Http\Request;
use App\Http\Resources\ProductsResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Imports\ProductImport;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function summary(){

        $orders = Orders::count();
        $suppliers = Suppliers::count();
        $products = Products::count();

        return [
            'orders' => $orders,

        ];

     }
    public function index()
    {
        //

        $productsQuery =  ProductsResource::collection(Products::all());
        return [
            'products' => $productsQuery
        ];

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $products = Products::create([
            'name' => $params['name'],
            'description' => $params['description'],
            'quantity' => $params['quantity'],
        ]);
        new ProductsResource($products);
        return response()->json("Product has been added Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $products=Products::findorfail($id);
        $product = new ProductsResource($products);
        return [
            'product' => $product
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($id === null) {
            return response()->json(['error' => 'products not found'], 404);
        }else{
            $products=Products::findorfail($id);
                $params = $request->all();
              $products->update($params);
          new ProductsResource($products);

          return response()->json("Product has been updated Successfully");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products=Products::find($id);
     $orders = OrderDetails::where('product_id',$id)->delete();
     $supplier_products = SupplierProducts::where('product_id',$id)->delete();



        if ($products->delete()) {
            return response()->json('Product deleted Succefully');
        } else {
            abort(404,'Product not found.');
        }
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new ProductImport,request()->file('file'));

        return back();
    }
}
