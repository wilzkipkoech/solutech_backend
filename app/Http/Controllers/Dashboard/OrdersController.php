<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\OrderDetails;
use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrdersController extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $ordersQuery = OrderResource::collection(Orders::all());
        return [
            'orders' => $ordersQuery
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $orders = Orders::create([
            'order_number' => $params['order_number'],

        ]);
        new OrderResource($orders);
        return response()->json("Order has been added Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $orders=Orders::findorfail($id);
        return new OrderResource($orders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($id === null) {
            return response()->json(['error' => 'order not found'], 404);
        }else{
            $orders=Orders::findorfail($id);
                $params = $request->all();
              $orders->update($params);
          new OrderResource($orders);

          return response()->json("Order has been updated Successfully");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders=Orders::find($id);
        $order_details = OrderDetails::where('order_id',$id)->delete();

        if ($orders->delete()) {
            return response()->json('Order deleted Succefully');
        } else {
            abort(404,'Order not found.');
        }
    }
}
