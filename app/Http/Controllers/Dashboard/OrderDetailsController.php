<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\OrderDetails;
use App\Models\Orders;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Resources\OrderDetailsResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use DB;

class OrderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $order_details = OrderDetails::all();
        $order_products = DB::table('order_details')
        ->select('order_details.id as order_details_id','products.name as product_name','order_number','product_id','order_id','order_details.created_at')
        ->join('products','products.id','=','order_details.product_id')
        ->join('orders','orders.id','=','order_details.order_id')
        ->get();
         return [
            'order_products' =>$order_products
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->id;

        $pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 6);
        $orders = Orders::create([
            'order_number' =>  $pass,
        ]);

           $order_id= $orders->id;

            $order_details = OrderDetails::create([
            'order_id' => $order_id,
            'product_id' =>  3,
        ]);


        new OrderDetailsResource($order_details);
        return response()->json("Order product details has been added Successfully");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $order_details = OrderDetails::findorfail($id);

        $order_products = DB::table('order_details')
        ->select('order_details.id as order_details_id','products.name as product_name','order_number','product_id','order_id')
        ->join('products','products.id','=','order_details.product_id')
        ->join('orders','orders.id','=','order_details.order_id')
        ->where('orders.',$id)
        ->get();
        // $order_detail =   new OrderDetailsResource($order_details);
        return [
            'order_detail' => $order_products
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderDetails $orderDetails)
    {
        //
        if ($id === null) {
            return response()->json(['error' => 'Order details not found'], 404);
        }else{
            $order_details=OrderDetails::findorfail($id);
                $params = $request->all();
              $order_details->update($params);
          new OrderDetailsResource($products);

          return response()->json("Order details  has been updated Successfully");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order_details=OrderDetails::find($id);

        if ($order_details->delete()) {
            return response()->json('Order details deleted Succefully');
        } else {
            abort(404,'Product not found.');
        }
    }
}
