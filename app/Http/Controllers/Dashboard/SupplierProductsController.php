<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\SupplierProducts;
use Illuminate\Http\Request;
use App\Http\Resources\SuppliersProductResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use DB;

class SupplierProductsController extends Controller
{/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $supplier_products = DB::table('supplier_products')
        ->select('supplier_products.id as supplier_products_id','products.name as product_name','suppliers.name as supplier_name','product_id','supplier_id')
        ->join('products','products.id','=','supplier_products.product_id')
        ->join('suppliers','suppliers.id','=','supplier_products.supplier_id')
        ->get();

         return [
            'supplier_products' =>$supplier_products
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $order_details = SupplierProducts::create([
        'supplier_id' => $params['supplier_id'],
        'product_id' => $params['product_id'],
    ]);

    new SuppliersProductResource($order_details);
    return response()->json("Supplier product details has been added Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $supplier_products=SupplierProducts::findorfail($id);
        return new ProductsResource($supplier_products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($id === null) {
            return response()->json(['error' => 'products not found'], 404);
        }else{
            $supplier_products=SupplierProducts::findorfail($id);
                $params = $request->all();
              $supplier_products->update($params);
          new ProductsResource($products);

          return response()->json("Product supplier has been updated Successfully");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier_products=SuppliersProductResource::find($id);

        if ($supplier_products->delete()) {
            return response()->json('Product supplier deleted Succefully');
        } else {
            abort(404,'Product not found.');
        }
    }
}
