<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Products;

use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsExport implements FromCollection , WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return Products::select('id','name','description','quantity')
         ->get();;
    }
    public function headings(): array
    {
        return [
            'Product Id',
            'Product Name',
            'Product Description',
            'Product Quantity',

        ];
    }
}
