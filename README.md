<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">


## About Solutech Backend

I have hosted  my backend solution the application on my test ubuntu server running on 
```
 http://196.216.82.145:56

 ```



## Key Features

Used Laravel Passport for authenticating my APIS

Crud functionality for 
  Products

  Suppliers

  Supplier Products

  Orders

  Order Details

  Allows genarating of reports by downloading a csv file


