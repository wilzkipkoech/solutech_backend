<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function() {

    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->middleware('guest');
    Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register'])->middleware('guest');

    Route::group(['middleware' =>'auth:api'], function() {

        Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);

        //routes for products
        Route::get('/summary', [App\Http\Controllers\Dashboard\ProductsController::class, 'summary']);
        Route::get('/products', [App\Http\Controllers\Dashboard\ProductsController::class, 'index']);
        Route::get('/products/create', [App\Http\Controllers\Dashboard\ProductsController::class, 'create']);
        Route::post('/products', [App\Http\Controllers\Dashboard\ProductsController::class, 'store']);
        Route::get('/products/{id}', [App\Http\Controllers\Dashboard\ProductsController::class, 'show']);
        Route::post('/products/{id}', [App\Http\Controllers\Dashboard\ProductsController::class, 'update']);
        Route::delete('/products/delete/{id}', [App\Http\Controllers\Dashboard\ProductsController::class, 'destroy']);



        // Export Import Controller
        Route::get('importExportView', [ App\Http\Controllers\Dashboard\ProductsController::class,  'importExportView' ]);
        Route::get('export', [ App\Http\Controllers\Dashboard\ProductsController::class,  'export' ])->name('export');
        Route::post('import', [ App\Http\Controllers\Dashboard\ProductsController::class,  'import' ])->name('import');

         //routes for suppliers
         Route::get('/supplier', [App\Http\Controllers\Dashboard\SuppliersController::class, 'index']);
         Route::get('/supplier/create', [App\Http\Controllers\Dashboard\SuppliersController::class, 'create']);
         Route::post('/supplier', [App\Http\Controllers\Dashboard\SuppliersController::class, 'store']);
         Route::get('/supplier/{id}', [App\Http\Controllers\Dashboard\SuppliersController::class, 'show']);
         Route::get('/supplier/edit/{id}', [App\Http\Controllers\Dashboard\SuppliersController::class, 'edit']);
         Route::post('/supplier/{id}', [App\Http\Controllers\Dashboard\SuppliersController::class, 'update']);
         Route::delete('/supplier/delete/{id}', [App\Http\Controllers\Dashboard\SuppliersController::class, 'destroy']);

       //routes for orders
        Route::get('/orders', [App\Http\Controllers\Dashboard\OrdersController::class, 'index']);
        Route::get('/orders/create', [App\Http\Controllers\Dashboard\OrdersController::class, 'create']);
        Route::post('/orders', [App\Http\Controllers\Dashboard\OrdersController::class, 'store']);
        Route::get('/orders/{id}', [App\Http\Controllers\Dashboard\OrdersController::class, 'show']);
        Route::get('/orders/edit/{id}', [App\Http\Controllers\Dashboard\OrdersController::class, 'edit']);
        Route::post('/orders/{id}', [App\Http\Controllers\Dashboard\OrdersController::class, 'update']);
        Route::delete('/orders/delete/{id}', [App\Http\Controllers\Dashboard\OrdersController::class, 'destroy']);

       //routes for order details
       Route::get('/orders-details', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'index']);
       Route::get('/orders/create', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'create']);
       Route::post('/orders-details', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'store']);
       Route::get('/orders-details/{id}', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'show']);
       Route::get('/orders/edit/{id}', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'edit']);
       Route::post('/orders-details/{id}', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'update']);
       Route::delete('/orders-details/delete/{id}', [App\Http\Controllers\Dashboard\OrderDetailsController::class, 'destroy']);


       //routes for suppliers products

       Route::get('/supplier-products', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'index']);
       Route::get('/supplier/create', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'create']);
       Route::post('/supplier-products', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'store']);
       Route::get('/supplier-products/{id}', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'show']);
       Route::get('/supplier/edit/{id}', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'edit']);
       Route::post('/supplier-products/{id}', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'update']);
       Route::delete('/supplier-products/delete/{id}', [App\Http\Controllers\Dashboard\SupplierProductsController::class, 'destroy']);



    });
});




